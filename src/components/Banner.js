import '../styles/Banner.css'

function Banner({ children }) {
    return <div className='jungle-banner'>{children}</div>
}

export default Banner
