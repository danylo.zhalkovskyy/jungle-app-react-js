import '../styles/Categories.css'

function Categories({ categories, activeCategory, setActiveCategory }) {
    return (
        <div className='jungle-categories'>
            <select
                value={activeCategory}
                onChange={(e) => setActiveCategory(e.target.value)}
                className='lmj-categories-select'
            >
                <option value=''>---</option>
                {categories.map((cat) => (
                    <option key={cat} value={cat}>
                        {cat}
                    </option>
                ))}
            </select>
            <button onClick={() => setActiveCategory('')}>Reset</button>
        </div>
    )
}

export default Categories
