import { useState } from 'react'

import '../styles/Footer.css'
import QuestionForm from "./QuestionForm";

function Footer() {
    const [inputValue, setInputValue] = useState('')

    function handleInput(e) {
        setInputValue(e.target.value)
    }

    function handleBlur() {
        if (!inputValue.includes('@')) {
            alert('Attention, there is no "@", this is not a valid address.')
        }
    }

    return (
        <footer className='jungle-footer'>
            <div>
                For plants lovers 🌿🌱🌵
            </div>
            <div>
                <QuestionForm />
            </div>
            <div className='jungle-e-mail'>
                <div style={{marginRight: '5px'}}>Your email: </div>
                <input
                    type="text"
                    placeholder='Enter your email'
                    value={inputValue}
                    onChange={handleInput}
                    onBlur={handleBlur}
                />
            </div>
        </footer>
    )
}

export default Footer
