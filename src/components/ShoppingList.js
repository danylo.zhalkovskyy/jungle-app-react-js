import { useState } from "react";

import { plantsList } from "../data/plantsList"

import PlantItem from "./PlantItem"
import Categories from "./Categories"

import '../styles/ShoppingList.css'

function ShoppingList({cart, updateCart}) {

    const [activeCategory, setActiveCategory] = useState('')

    const categories = plantsList.reduce(
        (acc, plant) =>
            acc.includes(plant.category) ? acc : acc.concat(plant.category),
        []
    )

    function addToCart(id, name, price) {
        const currentPlantAdded = cart.find((plant) => plant.id === id)

        if (currentPlantAdded) {
            const cartFilteredCurrentPlant = cart.filter(
                (plant) => plant.name !== name
            )
            updateCart([
                ...cartFilteredCurrentPlant,
                {id, name, price, amount: currentPlantAdded.amount + 1}
            ])
        } else {
            updateCart([
                ...cart,
                {id, name, price, amount: 1}
            ])
        }
    }

    return (
        <div className='jungle-shopping-list'>
            <div className='categories-items'>
                <div className='categories-list'>
                    <strong>Categories</strong>
                    <ul>
                        {categories.map((cat) => (
                            <li key={cat}>{cat}</li>
                        ))}
                    </ul>
                </div>
                {/*<div className='plants-list'>*/}
                {/*    <strong>Plants</strong>*/}
                {/*    <ul>*/}
                {/*        {plantsList.map((plant) => (*/}
                {/*            <li key={plant.id}>*/}
                {/*                { plant.name }*/}
                {/*                { (plant.isBestSale || plant.category === "plante grasse") && <span> 🔥</span> }*/}
                {/*                { plant.isSpecialOffer && <span> 🏷</span> }*/}
                {/*                <CareScale scaleValue={plant.water} careType='water' />*/}
                {/*                <CareScale scaleValue={plant.light} careType='light' />*/}
                {/*            </li>*/}
                {/*        ))}*/}
                {/*    </ul>*/}
                {/*</div>*/}
                <div className='sales-list'>
                    <strong>Sales</strong>
                    <ul>
                        {plantsList.map((plant) => (
                            plant.isSpecialOffer &&
                            <li key={plant.id}>
                                { plant.name }
                            </li>
                        ))}
                    </ul>
                </div>
                <div className='best-seller-list'>
                    <strong>Best seller</strong>
                    <ul>
                        {plantsList.map((plant) => (
                            plant.isBestSale &&
                            <li key={plant.id}>
                                { plant.name }
                            </li>
                        ))}
                    </ul>
                </div>
            </div>

            <div className='plants-items'>
                <strong>Plants items</strong>
                <Categories
                    categories={categories}
                    activeCategory={activeCategory}
                    setActiveCategory={setActiveCategory}
                />
                <div className='second-plants-shopping-list'>
                    {plantsList.map(({ id, cover, name, water, light, price, category }) => (
                        !activeCategory || activeCategory === category ? (
                            <div className='plant-item-from-list' key={id}>
                                <PlantItem
                                    id={id}
                                    cover={cover}
                                    name={name}
                                    water={water}
                                    light={light}
                                    price={price}
                                    category={category}
                                />
                                <button style={{width: "70px", marginLeft: "10px"}} onClick={() => addToCart(id, name, price)}>Add</button>
                            </div>
                        ) : null
                    ))}
                </div>
            </div>
        </div>
    )
}

export default ShoppingList
