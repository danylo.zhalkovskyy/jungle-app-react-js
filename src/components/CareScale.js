import Sun from '../assets/sun.svg'
import Water from '../assets/water.svg'

const quantityLabel = {
    1: 'few',
    2: 'moderate',
    3: 'many'
}

function CareScale({ scaleValue, careType }) {
    const range = [1, 2, 3]
    const scaleType =
        careType === 'light' ? (
            <img src={Sun} alt="sun-icon"/>
        ) : (
            <img src={Water} alt="water-icon"/>
        )

    return (
        <>
            { range.map((rangeElem) =>
                scaleValue >= rangeElem ? (
                    <span style={{marginRight: "5px"}} key={rangeElem.toString()} onClick={() => alert(`The plant needs ${quantityLabel[scaleValue]} ${
                        careType === 'light' ? 'of light' : "watering"
                    }`)}>{scaleType}</span>
                ) : null
            )}
        </>
    )
}

export default CareScale
