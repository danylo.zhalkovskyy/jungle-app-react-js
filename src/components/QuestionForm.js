import { useState } from 'react'

function QuestionForm() {
    const [inputValue, setInputValue] = useState('Ask your question here ?')

    function checkValue(value) {
        if (!value.includes('f')) {
            setInputValue(value)
        }
    }

    return (
        <div style={{display: "flex", flexDirection: "row"}}>
            <textarea
                rows={1}
                cols={30}
                value={inputValue}
                onChange={(e) => checkValue(e.target.value)}
            />
            <br/>
            <button onClick={() => alert(inputValue)} style={{marginLeft: "5px"}}>Alert me 🚨</button>
        </div>
    )
}

export default QuestionForm
