import { useState, useEffect } from "react"

import '../styles/Cart.css'

const Cart = ({cart, updateCart}) => {
    const [isOpen, setIsOpen] = useState(true)

    const total = cart.reduce(
        (acc, plantType) => acc + plantType.amount * plantType.price,
        0
    )

    function handleRemove(id, name, price) {
        const currentPlantRemove = cart.find((plant) => plant.id === id)

        if (currentPlantRemove) {
            const newList = cart.filter(
                (item) => item.name !== name
            )
            if (currentPlantRemove.amount !== 1) {
                updateCart([
                    ...newList,
                    {id, name, price, amount: currentPlantRemove.amount - 1}
                ])
            } else {
                updateCart([
                    ...newList
                ])
            }
        }
    }

    function compare(a, b) {
        if (a.name < b.name){
            return -1;
        }
        if (a.name > b.name){
            return 1;
        }
        return 0;
    }
    cart.sort(compare);

    useEffect(() => {
        document.title = (`Total : ${total}€ 💸`)
        // alert(`I will have ${total}€ to pay 💸`)
    }, [total])

    return isOpen ? (
        <div className='jungle-cart'>
            <button style={{marginBottom: "10px"}} className='jungle-cart-toggle-button' onClick={() => setIsOpen(false)}>
                Close
            </button>
            {cart.length > 0 ? (
                <div>
                    <h2>My Cart</h2>
                    {cart.map(({id, name, price, amount}) =>
                        <div key={id}>
                            {name} {price}€ x <strong>{amount}</strong>
                        <br/>
                            <button style={{margin: "5px 0 5px 0"}} onClick={() => handleRemove(id, name, price)}>
                                Remove
                            </button>
                        </div>
                    )}
                    <h3>Total : {total}€</h3>
                    <button onClick={() => updateCart([])}>
                        Clear my Cart
                    </button>
                </div>
            ) : (
                <div>Your Cart is <strong>empty</strong></div>
            )}
        </div>
    ) : (
        <div className='jungle-cart'>
            <button className='jungle-cart-toggle-button' onClick={() => setIsOpen(true)}>
                Open Cart
            </button>
        </div>
    )
}

export default Cart
