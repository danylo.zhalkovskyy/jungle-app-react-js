import { useState, useEffect } from "react"

import Banner from './Banner'
import Cart from './Cart'
import ShoppingList from "./ShoppingList"
import Footer from "./Footer"

import logo from '../assets/leaf.png'
import '../styles/Layout.css'

function App() {
    // The localStorage knows only one type of value: strings
    const savedCart = localStorage.getItem('cart')
    const [cart, updateCart] = useState(savedCart ? JSON.parse(savedCart) : [])

    useEffect(() => {
        localStorage.setItem('cart', JSON.stringify(cart))
    }, [cart])

    return (
      <div>
          <Banner>
              <img src={logo} alt='jungle-img' className='jungle-logo' />
              <h1 className='jungle-title'>The Jungle</h1>
          </Banner>
          <div className='jungle-layout-inner'>
            <Cart cart={cart} updateCart={updateCart} />
            <ShoppingList cart={cart} updateCart={updateCart} />
          </div>
          <Footer />
      </div>
    )
}

export default App
