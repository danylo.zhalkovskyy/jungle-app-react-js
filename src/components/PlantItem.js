import CareScale from "./CareScale";

import '../styles/PlantItem.css'

function handleClick(plantName) {
    alert(`You want to buy 1 ${plantName} ? Great choice 🌱✨`)
}

function PlantItem({ id, cover, name, water, light, price }) {
    return (
        <div key={id} className='jungle-plant-item' onClick={() => handleClick(name)}>
            <span className='jungle-plant-item-price'>{price}€</span>
            <img className='jungle-plant-item-cover' src={cover} alt={`${name}-cover`}/>
            Name: {name}
            <div>
                Water: <CareScale careType='water' scaleValue={water} />
                Light: <CareScale careType='light' scaleValue={light} />
            </div>
        </div>
    )
}

export default PlantItem
