import monstera from '../assets/monstera.jpg'

export const plantsList = [
    {
        name: 'monstera',
        category: 'classic',
        id: '1ed',
        isBestSale: true,
        light: 2,
        water: 3,
        cover: monstera,
        price: 11
    },
    {
        name: 'ficus lyrata',
        category: 'classic',
        id: '2ab',
        light: 3,
        water: 1,
        cover: monstera,
        price: 9
    },
    {
        name: 'silver pothos',
        category: 'classic',
        id: '3sd',
        light: 1,
        water: 2,
        cover: monstera,
        price: 14
    },
    {
        name: 'yucca',
        category: 'classic',
        id: '4kk',
        isSpecialOffer: true,
        light: 3,
        water: 1,
        cover: monstera,
        price: 17
    },
    {
        name: 'olive tree',
        category: 'outdoor',
        id: '5pl',
        light: 3,
        water: 1,
        cover: monstera,
        price: 15
    },
    {
        name: 'geranium',
        category: 'outdoor',
        id: '6uo',
        isSpecialOffer: true,
        light: 2,
        water: 2,
        cover: monstera,
        price: 22
    },
    {
        name: 'basil',
        category: 'outdoor',
        id: '7ie',
        light: 2,
        water: 3,
        cover: monstera,
        price: 5
    },
    {
        name: 'aloe',
        category: 'succulent',
        id: '8fp',
        isBestSale: true,
        light: 2,
        water: 1,
        cover: monstera,
        price: 3
    },
    {
        name: 'haworthia',
        category: 'succulent',
        id: '9vn',
        isBestSale: true,
        light: 2,
        water: 1,
        cover: monstera,
        price: 12
    }
]
